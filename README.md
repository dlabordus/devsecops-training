# devsecops-training

The Introduction to DevSecOps training powered by Codecentric/VX Company

## Pull the docker images needed for the training

    docker pull araido/devsecops-jenkins
    docker pull hadolint/hadolint
    docker pull owasp/zap2docker-stable

## Run the optimized Jenkins Docker container on port 7070 and mount it to a local file folder
Make a temporary directory to store the Jenkins configuration. Open a command prompt and go to this directory.

    PWD=`pwd`
    docker run --name=jenkins -e "JAVA_OPTS=-Djenkins.install.runSetupWizard=false" -d -p 7070:8080 -v /var/run/docker.sock:/var/run/docker.sock -v $PWD:/var/jenkins_home araido/devsecops-jenkins

For Windows: Go to Docker settings and then to 'Shared Drives' and add your drive, then:

    docker run --name=jenkins -e "JAVA_OPTS=-Djenkins.install.runSetupWizard=false" -d -p 7070:8080 -v /var/run/docker.sock:/var/run/docker.sock -v c:/Users/<YOUR_FOLDER>:/var/jenkins_home araido/devsecops-jenkins

## Login to jenkins

Navigate to "http://localhost:7070" and login as "admin" with password "admin".

## Create a new Jenkins Job

Click on "create new jobs". 

Enter an item name for the new job, for instance "DevSecOps" and select as type "Pipeline". Click on "Ok" to create the Job.

## Checkout code from GitLab (once)
Go to the section "Pipeline" and add the following code block.

    node {
      stage('Checkout') {
        sh 'rm -rf devsecops-java'
        sh 'git clone https://gitlab.com/dlabordus/devsecops-java.git'
      }
      dir('devsecops-java') {
        stage('Compile') {
            sh './mvnw compile'
        }
      }
    }

Save the changes and run the new Job once using "Build now".

The code is now checked out and can be found locally on your local harddisk in "<CURRENT-DIRECTORY>/workspace/DevSecOps/devsecops-java".
To prevent that we need to commit and push changes we make to the code we will disable the rm/git clone part of the Job.
Changes will be done directly in the workspace of Jenkins to make it easier for this workshop. 
We will now disable the stage "Checkout". Edit the configuration of the Jenkins Job and add "//" in front of the lines of the stage "Checkout". Click "save" to store the change.

    node {
      stage('Checkout') {
    //    sh 'rm -rf devsecops-java'
    //    sh 'git clone https://gitlab.com/dlabordus/devsecops-java.git'
      }
      dir('devsecops-java') {
        stage('Compile') {
            sh './mvnw compile'
        }
      }
    }

## Step 1: Add Findsecbugs to scan java code

Add the following plugin to the Build section in the Maven POM. The POM file can be found on your local harddisk: <CURRENT-DIRECTORY>/workspace/DevSecOps/devsecops-java/pom.xml

    <plugin>
        <groupId>com.github.spotbugs</groupId>
        <artifactId>spotbugs-maven-plugin</artifactId>
        <version>3.1.6</version>
        <configuration>
            <effort>Max</effort>
            <threshold>low</threshold>
            <failOnError>true</failOnError>
            <includeFilterFile>${session.executionRootDirectory}/spotbugs-security-include.xml</includeFilterFile>
            <excludeFilterFile>${session.executionRootDirectory}/spotbugs-security-exclude.xml</excludeFilterFile>
            <plugins>
                <plugin>
                    <groupId>com.h3xstream.findsecbugs</groupId>
                    <artifactId>findsecbugs-plugin</artifactId>
                    <version>LATEST</version> <!-- Auto-update to the latest stable -->
                </plugin>
            </plugins>
        </configuration>
    </plugin>

Edit the Jenkins Job and add the following section after the stage "Compile".

    stage('FindSecBugs') {
        sh './mvnw spotbugs:check'
    }

Run the Job and check the logging. Go to https://find-sec-bugs.github.io/bugs.htm and look for the security bugs. Try to fix the problem in the Java Code.

## Step 2: Add OWASP Dependency Checker to scan maven dependencies used

Add the following plugin to the Build section in the Maven POM. File is <CURRENT-DIRECTORY>/workspace/DevSecOps/devsecops-java/pom.xml
            
    <plugin>
        <groupId>org.owasp</groupId>
        <artifactId>dependency-check-maven</artifactId>
        <version>3.3.4</version>
        <configuration>
            <failBuildOnCVSS>6</failBuildOnCVSS>
        </configuration>
    </plugin>

Edit the Jenkins Job and add the following section after previous stage.

    stage('Dependency Checker') {
        sh './mvnw dependency-check:check'
    }

Run the Job and check the logging. Try to fix the problem in the Maven POM.

## Step 3: Use Hado-lint to check docker file

Edit the Jenkins Job and add the following section after previous stage.

    stage('Docker File Checker') {
        sh 'docker run --rm -i hadolint/hadolint < Dockerfile'
    }

Run the Job and check the logging. See https://github.com/hadolint/hadolint for more information about Hadolint. Try to fix the problem in the Docker File.

## Build Docker image and run application

Now that everything is secure, let's build the docker image and run the application for the final step.

Edit the Jenkins Job and add the following section after previous stage.

    stage('Build') {
        sh './mvnw install -DskipTests dockerfile:build'
    }
    stage ('Run Docker image') {
        sh 'docker run -d -p 8080:8080 --name spring -t springio/gs-spring-boot-docker'
    }

The Docker container is now running. If the Job is executed more then once this will be a problem. So as a shortcut we will stop the Docker container when the Jenkins job is started. Add the following stage as first stage to the Jenkins Job before the stage "Compile".

    stage('Clean up') {
        sh '''
           if [ `docker ps -aq --filter name=spring | wc -l` -ge 1 ] 
           then
             docker rm -f $(docker ps -aq --filter name=spring)
           fi
        '''
    }

## Step 4: Last Step using ZAP to scan the running container

Edit the Jenkins Job and add the following section after the "Run Docker image" stage.

    stage('ZAP') {
        sh '''
            export SPRING=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' spring)
            docker run --rm -t owasp/zap2docker-stable zap-baseline.py -z "-config api.disablekey=true" -t http://${SPRING}:8080
        '''
    }

Run the Job and check the logging. Try to fix the problem in the Java Code. See https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project for information about OWASP ZAP.
