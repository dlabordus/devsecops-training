Pipeline

Step 1
node {
    stage('Checkout') { 
        sh 'git clone https://gitlab.araido.com/training/devsecops-java'
    }
}

Step 2  - SAST

First pom.xml ⇒

          <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
                <version>3.1.6</version>
                <configuration>
                    <effort>Max</effort>
                    <threshold>low</threshold>
                    <failOnError>true</failOnError>
                    <includeFilterFile>${session.executionRootDirectory}/spotbugs-security-include.xml</includeFilterFile>
                    <excludeFilterFile>${session.executionRootDirectory}/spotbugs-security-exclude.xml</excludeFilterFile>
                    <plugins>
                        <plugin>
                            <groupId>com.h3xstream.findsecbugs</groupId>
                            <artifactId>findsecbugs-plugin</artifactId>
                            <version>LATEST</version> <!-- Auto-update to the latest stable -->
                        </plugin>
                    </plugins>
                </configuration>
            </plugin>



node {
    stage('Checkout') { 
    }
    dir('devsecops-java'){
        stage('SAST') {
    	sh './mvnw compile spotbugs:check'
        }
     stage ('Dependency check'){
           sh './mvnw dependency-check:check'
     }
    }
}


Go to https://find-sec-bugs.github.io/bugs.htm and look for the security bugs:

SPRING_UNVALIDATED_REDIRECT
Do not use the user supplied input for your redirect 



SPRING_CSRF_UNRESTRICTED_REQUEST_MAPPING
Fix: Use @GetMapping from org.springframework.web.bind.annotation.GetMapping;


Step 3 - Dependency management

Add in POM.xml =>
            <plugin>
              <groupId>org.owasp</groupId>
              <artifactId>dependency-check-maven</artifactId>
              <version>3.3.1</version>
              <configuration>
                <failBuildOnCVSS>4</failBuildOnCVSS>
              </configuration>
            </plugin>








node {
    stage('Checkout') { 
    }
    dir('devsecops-java'){
        stage('SAST') {
    	sh './mvnw compile spotbugs:check'
        }
     stage ('Dependency check'){
           sh './mvnw dependency-check:check'
     }
      stage ('Check dockerfile'){
            sh 'docker run --rm -i araido/devsecops-hadolint < Dockerfile' 
      }

    }
}

Fix: Upgrade library, save 143 million people

Step 4 - Dockerfile check

node {
    stage('Checkout') { 
    }
    dir('devsecops-java'){
        stage('SAST') {
    	sh './mvnw compile spotbugs:check'
        }
     stage ('Dependency check'){
           sh './mvnw dependency-check:check'
     }
      stage ('Check dockerfile'){
            sh 'docker run --rm -i araido/devsecops-hadolint < Dockerfile' 
      }

    }
}

Check errors:  https://github.com/hadolint/hadolint/wiki






Step 5 - Build & Run our docker container
node {
    stage('Checkout') { 
    }
    dir('devsecops-java'){
        stage('SAST') {
    	sh './mvnw compile spotbugs:check'
        }
     stage ('Dependency check'){
           sh './mvnw dependency-check:check'
     }
      stage ('Check dockerfile'){
            sh 'docker run --rm -i araido/devsecops-hadolint < Dockerfile' 
      }
	stage('Build') { 
            sh './mvnw install -DskipTests dockerfile:build'
        }
        stage ('Run Docker image') {
            sh 'docker run -d -p 8080:8080 --name spring -t springio/gs-spring-boot-docker'
        }
    }
}



Step 5 - OWASP ZAP

node {
    sh 'docker stop $(docker ps -a --filter name=spring -q )'
    sh 'docker rm $(docker ps -a --filter name=spring -q )'
    stage('Checkout') { 
    }
    dir('devsecops-java'){
        stage('SAST') {
    	sh './mvnw compile spotbugs:check'
        }
     stage ('Dependency check'){
           sh './mvnw dependency-check:check'
     }
      stage ('Check dockerfile'){
            sh 'docker run --rm -i araido/devsecops-hadolint < Dockerfile' 
      }
	stage('Build') { 
            sh './mvnw install -DskipTests dockerfile:build'
        }
        stage ('Run Docker image') {
            sh 'docker run -d -p 8080:8080 --name spring -t springio/gs-spring-boot-docker'
        }
	        stage('DAST') { 
           sh '''
           export SPRING=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' spring)
           docker run --rm -t araido/devsecops-zap zap-baseline.py -t http://${SPRING}:8080
           '''
        }
    }
}


Fix the issue:

import javax.servlet.http.HttpServletResponse;

   @GetMapping("/")
   public String home(HttpServletResponse response) {
       response.addHeader("X-Content-Type-Options", "nosniff");
       return "What an awesome DevSecOps training!" + UDecoder.URLDecode("www.google.com");